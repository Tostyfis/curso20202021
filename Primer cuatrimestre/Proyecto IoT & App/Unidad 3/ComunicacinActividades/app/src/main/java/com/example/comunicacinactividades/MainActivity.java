package com.example.comunicacinactividades;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private Button verificar;
    private EditText entrada;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        verificar = findViewById(R.id.buttonVerficar);
        verificar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                lanzarVerificar(null);
            }
        });

    }
    public void lanzarVerificar(View view){
        Intent i = new Intent(this, verificacion.class);
        entrada = findViewById(R.id.entrada);
        i.putExtra("usuario", entrada.getText().toString());
        startActivity(i);
    }

}

