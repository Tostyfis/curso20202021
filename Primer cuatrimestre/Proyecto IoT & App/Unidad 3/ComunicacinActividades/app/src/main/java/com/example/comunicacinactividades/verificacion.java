package com.example.comunicacinactividades;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class verificacion extends Activity {

    @Override public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.verificar);
        Bundle extras = getIntent().getExtras();
        String s = extras.getString("usuario");
        TextView texto = findViewById(R.id.textView);
        texto.setText("Hola " + s + ", ¿Aceptas las condiciones?");
    }

}
