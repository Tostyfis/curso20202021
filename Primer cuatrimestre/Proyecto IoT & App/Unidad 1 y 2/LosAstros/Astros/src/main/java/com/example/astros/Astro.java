package com.example.astros;

public class Astro {

    //Definimos atributos

    private String nombre;

    private String tipo;

    private double brillo;

    private double distancia;

    //Definimos metodos

    public Astro(){

        nombre = "Sirius";

        tipo = "ESTRELLA";

        brillo = -1.42;

        distancia = 8.7;

    }

    public Astro(String nombre, String tipo, double brillo, double distancia ){

        if(tipo.equals("ESTRELLA") || tipo.equals("NEBULOSA") || tipo.equals("GALAXIA")){

            this.nombre = nombre;

            this.tipo = tipo;

            this.brillo = brillo;

            this.distancia = distancia;



        }else{

            this.nombre = "";

            this.tipo = "";

            this.brillo = 0.0;

            this.distancia = 0.0;


        }


    }

    //Getters y setters

    public double getBrillo() {

        return brillo;

    }

    public void setBrillo(double brillo) {

        this.brillo = brillo;

    }

    //Definimos métodos

    public boolean masDistante(Astro otro){

        if(this.distancia > otro.distancia ){

            return true;

        }else{

            return false;

        }

    }

    public int masBrillante(Astro otro){

        if(this.brillo > otro.brillo ){

            return 1;

        }else if(this.brillo < otro.brillo){

            return -1;

        }else{

            return 0;

        }

    }

    public String visibleCon(){

        if(this.brillo < 5){

            return "a simple vista";

        }else if(this.brillo >= 5 && this.brillo > 7){

            return "con prismáticos";

        }else if(this.brillo >= 7 && this.brillo > 25){

            return "con telescopio";

        }else{

            return "con grandes telescopios";

        }

    }

    public boolean equals(Astro otro){

        if(this.nombre.equals(otro.nombre) == true && this.tipo.equals(otro.tipo) == true && this.brillo == otro.brillo && this.distancia == otro.distancia){

               return true;

        }else{

            return false;

        }

    }

    public String toString(){

        return "Nombre: " + nombre + " Tipo: " + tipo + " Brillo: " + brillo + " Distancia " + distancia;

    }

}