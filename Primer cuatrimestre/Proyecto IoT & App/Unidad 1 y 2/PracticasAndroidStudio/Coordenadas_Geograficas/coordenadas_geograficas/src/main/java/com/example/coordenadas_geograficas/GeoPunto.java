/** Clase que representa coordenadas_geograficas. */

package com.example.coordenadas_geograficas;

class GeoPunto {

    //declaracion de atributos

    private double longitud, latitud;

    //declaracion del constructor

    public GeoPunto(double longitud, double latitud){

        this.longitud = longitud;

        this.latitud = latitud;

    }
    //declaración de métodos

    /** Transcribe el complejo a String.
     * @returnun string con la parte entera y la imaginaria
     */

    public String toString(){

        return longitud + ":" + latitud + ";";

    }
    /** Define el método  double distancia(GeoPunto punto) para aproximar la distancia en metros entre dos
     * coordenadas. Podemos utilizar el algoritmo Haversine, cuya posible implementación en Java se muestra a continuacion
     */
    public double distancia(GeoPunto punto){

        final double RADIO_TIERRA = 6371000; // en metros
        double dLat = Math.toRadians(latitud - punto.latitud);
        double dLon = Math.toRadians(longitud - punto.longitud);
        double lat1 = Math.toRadians(punto.latitud);
        double lat2 = Math.toRadians(latitud);
        double a =    Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.sin(dLon/2) * Math.sin(dLon/2) *
                        Math.cos(lat1) * Math.cos(lat2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        return c * RADIO_TIERRA;

    }

}