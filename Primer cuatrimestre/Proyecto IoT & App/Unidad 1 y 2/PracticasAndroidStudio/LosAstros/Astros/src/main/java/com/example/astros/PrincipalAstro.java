package com.example.astros;

import java.util.Scanner;
import java.util.Locale;

public class PrincipalAstro {

    public static void main(String[] args) {

      Astro a1 = new Astro();

      Astro a2 = new Astro("FUEGO","SOL",3,3);

      Astro a3 = new Astro("Andrómeda", "GALAXIA", 6.7,2.0);

      Astro a4 = new Astro("Andrómeda", "GALAXIA", 6.7, 2.0);

      Astro a5 = new Astro("Centaurus","ESTRELLA",5.6,5.8);

      Astro a6 = new Astro("Centaurus","ESTRELLA",5.6,5.8);

      System.out.println(a1);

      System.out.println(a2);

      System.out.println(a3);

      System.out.println(a4);

      System.out.println(a5);

      a3.setBrillo(4);

      System.out.println(a3.getBrillo());

      System.out.println(a3.masDistante(a5));

      System.out.println(a4.masBrillante(a5));

      System.out.println(a5.visibleCon());

      System.out.println(a5.equals(a6));




    }


}
