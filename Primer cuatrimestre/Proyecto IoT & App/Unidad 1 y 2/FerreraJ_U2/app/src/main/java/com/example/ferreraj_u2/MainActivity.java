package com.example.ferreraj_u2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import androidx.viewpager2.widget.ViewPager2;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

import static java.lang.Integer.parseInt;

public class MainActivity extends AppCompatActivity {
    //==============================================================================================
    // Nombres de las pestañas
    private String[] nombres = new String[]{"Pestaña 1","Pestaña 2","Pestaña 3","Pestaña 4"};
    //==============================================================================================
    private TextView entrada;
    private TextView salida;
    private String operation;
    private int numero1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //==========================================================================
        entrada = findViewById(R.id.entrada);
        salida = findViewById(R.id.salida);
        //==========================================================================================
        //Pestañas
        //==========================================================================================
        ViewPager2 viewPager = findViewById(R.id.viewpager);
        viewPager.setAdapter(new MiPagerAdapter(this));
        TabLayout tabs = findViewById(R.id.tabs);
        new TabLayoutMediator(tabs, viewPager,
                new TabLayoutMediator.TabConfigurationStrategy() {
                    @Override
                    public void onConfigureTab(@NonNull TabLayout.Tab tab, int position){
                        tab.setText(nombres[position]);
                    }
                }
        ).attach();
        //==========================================================================================
    }
    //==============================================================================================
    //Taps
    //==============================================================================================
    public class MiPagerAdapter extends FragmentStateAdapter {
        public MiPagerAdapter(FragmentActivity activity){
            super(activity);
        }
        @Override
        public int getItemCount() {
            return 4;
        }
        @Override @NonNull
        public Fragment createFragment(int position) {
            switch (position) {
                case 0: return new Tab1();
                case 1: return new Tab2();
                case 2: return new Tab3();
                case 3: return new Tab4();
            }
            return null;
        }
    }
    //==============================================================================================
    //Boton
    //==============================================================================================
    public void sePulsa(View view){
        Toast.makeText(this, "Pulsado", Toast.LENGTH_SHORT).show();
    }
    //==============================================================================================
    //Calculadora
    //==============================================================================================
    public void estadoDeEntrada () {

        entrada = findViewById(R.id.entrada);

        if (entrada.getText().toString().contains("=") ||

        entrada.getText().toString().contains("Entrada inválida") ||

        entrada.getText().toString().contains("Pts")) {

            salida.setText("");

        }
    }
    public void sePulsa0(View view){
        estadoDeEntrada();
        entrada.setText(entrada.getText()+(String)view.getTag());

    }
    public void sePulsa1(String texto){

        entrada = findViewById(R.id.entrada);

        salida = findViewById(R.id.salida);

        salida.setText(entrada.getText()+texto);

        entrada.setText(null);

    }
    public void eliminar(View view){
        estadoDeEntrada();

        entrada.setText(null);

    }
    public void eliminarTodo(View view){


        entrada.setText(null);
        salida.setText(null);

    }
    public void sePulsaPesetas(View view){

        salida.setText(String.valueOf(Float.parseFloat(

                entrada.getText().toString()) * 166.3860));

    }

    public void operacion(View view){

        estadoDeEntrada();

        numero1 = parseInt((String)entrada.getText());


        operation = (String)view.getTag();

        sePulsa1(operation);


    }

    public void igual(View view){

        estadoDeEntrada();

        String x = (String)entrada.getText();
        int res;
        switch (operation)
        {
            case "+":
                res = numero1 + parseInt(x);
                salida.setText(String.valueOf(res));
                entrada.setText(null);
                numero1 = res;
                break;
            case "-":
                res = numero1 - parseInt(x);
                salida.setText(String.valueOf(res));
                entrada.setText(null);
                break;
            case "*":
                res = numero1 * parseInt(x);
                salida.setText(String.valueOf(res));
                entrada.setText(null);
                break;
            case "/":
                if(parseInt(x) == 0){

                    Toast.makeText(this, "Syntax error", Toast.LENGTH_SHORT).show();
                    salida.setText(null);
                    entrada.setText(null);


                }else{
                    res = numero1 / parseInt(x);
                    salida.setText(String.valueOf(res));
                    entrada.setText(null);
                }
                break;
            default:
                break;
        }

    }

}