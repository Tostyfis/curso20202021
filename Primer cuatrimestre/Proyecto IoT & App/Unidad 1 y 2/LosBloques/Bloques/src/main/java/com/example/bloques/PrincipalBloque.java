package com.example.bloques;

import java.util.Scanner;
import java.util.Locale;

public class PrincipalBloque {

    public static void main(String[] args) {

    Bloque b1 = new Bloque();

    Bloque b2 = new Bloque(60);

    Bloque b3 = new Bloque("azul", 40);

    Bloque b4 = new Bloque("morado",-6);

    System.out.println(b1);

    System.out.println(b2);

    System.out.println(b3);

    System.out.println(b4);

    b3.setDimension(20);

    System.out.println(b3.getDimension());

    System.out.println(b4.equals(b4));

    System.out.println(b4.puedeEstarEncimaDe(b2));

    }
}
