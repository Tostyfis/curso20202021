package com.example.bloques;

import java.sql.Struct;

public class Bloque {

    /** definimos atributos
     */

    private String color;

    private int dimension;

    private boolean comodin;

    /** Definimos constructores
     */

    public Bloque(){

        color = "azul";

        dimension = 25;

        comodin = false;

    }

    public Bloque(int dimension){

        if(dimension > 50){

            this.dimension = 50;

        }else if(dimension < 1){

            this.dimension = 1;

        }else{

            this.dimension = dimension;

        }

        color = "";

        comodin = true;

    }

    public Bloque(String color, int dimension){

        if(dimension > 50){

            this.dimension = 50;

        }else if(dimension < 1){

            this.dimension = 1;

        }else{

            this.dimension = dimension;

        }

        if(color.equals("rojo") || color.equals("azul")){

            this.color = color;

            comodin = false;

        }else{

            this.color = "";

            this.dimension = 0;

            this.comodin = false;

        }


    }

    public int getDimension() {

        return dimension;

    }

    public void setDimension(int dimension) {

        if(dimension > 50){

            this.dimension = 50;

        }else if(dimension < 1){

            this.dimension = 1;

        }else{

            this.dimension = dimension;

        }

    }

    /** Definimos métodos
     */

    public String toString(){

        return "Comodin:" + comodin + "Color:" + color + "Dimension:" + dimension;

    }

    public boolean equals(Bloque a){

        if(this.color.equals(a.color) == true && this.dimension == a.dimension){

                return true;

        }else{

            return false;

        }

    }

    public boolean puedeEstarEncimaDe(Bloque a){

        if(this.color.equals(a.color) != true && this.dimension > a.dimension){

            return true;

        }else{

            return false;

        }


    }

}

