#include <M5Stack.h>

//define __WONDOWS__

#ifdef __WINDOWS__

#include <Arduino.h>

#define VARIABLE "adios"

#else

#include <unistd.h>

#define VARIABLE "Hola"

#endif 

void setup(){

  M5.begin();

  M5.Lcd.printf("Hello world");

  Serial.print(VARIABLE);
}

void loop(){
  
}
